import React from 'react';

const Toaster = React.lazy(() => import('./views/notifications/toaster/Toaster'));
const Tables = React.lazy(() => import('./views/rent/tables/Tables'));
const ListCustomer = React.lazy(() => import('./views/rent/customer/listCustomer'));
const CreateCustomer = React.lazy(() => import('./views/rent/customer/createCustomer'));
const RentProcess = React.lazy(() => import('./views/rent/rentProcess/creditOperations'));
const WizardFormSecond = React.lazy(() => import('./views/rent/rentProcess/wizardFormSecond'));
const ConfirmCredit = React.lazy(() => import('./views/rent/rentProcess/confirmCredit'));
const ConfirmCreditList = React.lazy(() => import('./views/rent/rentProcess/confirmCreditList'));
const PaymentInstructions = React.lazy(() => import('./views/rent/paymentInstructions/paymentInstructions'));
const Carousels = React.lazy(() => import('./views/rent/carousels/Carousels'));
const Collapses = React.lazy(() => import('./views/rent/collapses/Collapses'));
const BasicForms = React.lazy(() => import('./views/rent/forms/BasicForms'));

const Jumbotrons = React.lazy(() => import('./views/rent/jumbotrons/Jumbotrons'));
const ListGroups = React.lazy(() => import('./views/rent/list-groups/ListGroups'));
const Navbars = React.lazy(() => import('./views/rent/navbars/Navbars'));
const Navs = React.lazy(() => import('./views/rent/navs/Navs'));
const Paginations = React.lazy(() => import('./views/rent/paginations/Pagnations'));
const Popovers = React.lazy(() => import('./views/rent/popovers/Popovers'));
const ProgressBar = React.lazy(() => import('./views/rent/progress-bar/ProgressBar'));
const Switches = React.lazy(() => import('./views/rent/switches/Switches'));

const Tabs = React.lazy(() => import('./views/rent/tabs/Tabs'));
const Tooltips = React.lazy(() => import('./views/rent/tooltips/Tooltips'));
const BrandButtons = React.lazy(() => import('./views/buttons/brand-buttons/BrandButtons'));
const ButtonDropdowns = React.lazy(() => import('./views/buttons/button-dropdowns/ButtonDropdowns'));
const ButtonGroups = React.lazy(() => import('./views/buttons/button-groups/ButtonGroups'));
const Buttons = React.lazy(() => import('./views/buttons/buttons/Buttons'));
const Charts = React.lazy(() => import('./views/charts/Charts'));
const Dashboard = React.lazy(() => import('./views/dashboard/Dashboard'));
const CoreUIIcons = React.lazy(() => import('./views/icons/coreui-icons/CoreUIIcons'));
const Flags = React.lazy(() => import('./views/icons/flags/Flags'));
const Brands = React.lazy(() => import('./views/icons/brands/Brands'));
const Alerts = React.lazy(() => import('./views/notifications/alerts/Alerts'));
const Badges = React.lazy(() => import('./views/notifications/badges/Badges'));
const Modals = React.lazy(() => import('./views/notifications/modals/Modals'));
const Colors = React.lazy(() => import('./views/theme/colors/Colors'));
const Typography = React.lazy(() => import('./views/theme/typography/Typography'));
const Widgets = React.lazy(() => import('./views/widgets/Widgets'));
const Users = React.lazy(() => import('./views/users/Users'));
const User = React.lazy(() => import('./views/users/User'));

const routes = [
  { path: '/dashboard', name: 'Ana Sayfa', component: Dashboard },
  { path: '/theme', name: 'Theme', component: Colors, exact: true },
  { path: '/theme/colors', name: 'Colors', component: Colors },
  { path: '/theme/typography', name: 'Typography', component: Typography },
  { path: '/rent/creditOperations', name: 'Kredi Başvuru İşlemi', component: RentProcess },
  { path: '/rent/rentProcess/confirmCredit', name: 'Kredi Onay Ekranı', component: ConfirmCredit },
  { path: '/rent/rentProcess/ConfirmCreditList', name: 'Kredi Onay Ekranı', component: ConfirmCreditList },
  
  { path: '/rent/rentProcess/wizardFormSecond', name: 'Kredi Başvuru 2.Sayfa', component: WizardFormSecond },
  { path: '/rent/paymentInstructions', name: 'Ödeme Talimatı İşlemi', component: PaymentInstructions },
  { path: '/rent/customer/createCustomer', name: 'Müşteri Ekleme Ekranı', component: CreateCustomer },
  
  { path: '/base/carousels', name: 'Carousel', component: Carousels },
  { path: '/base/collapses', name: 'Collapse', component: Collapses },
  { path: '/base/forms', name: 'Forms', component: BasicForms },
  { path: '/rent/customer/listCustomer', name: 'Müşteri Ekranı', component: ListCustomer },
  { path: '/base/jumbotrons', name: 'Jumbotrons', component: Jumbotrons },
  { path: '/base/list-groups', name: 'List Groups', component: ListGroups },
  { path: '/base/navbars', name: 'Navbars', component: Navbars },
  { path: '/base/navs', name: 'Navs', component: Navs },
  { path: '/base/paginations', name: 'Paginations', component: Paginations },
  { path: '/base/popovers', name: 'Popovers', component: Popovers },
  { path: '/base/progress-bar', name: 'Progress Bar', component: ProgressBar },
  { path: '/base/switches', name: 'Switches', component: Switches },
  { path: '/rent/tables/Tables', name: 'Tables', component: Tables },
  { path: '/base/tabs', name: 'Tabs', component: Tabs },
  { path: '/base/tooltips', name: 'Tooltips', component: Tooltips },
  { path: '/buttons', name: 'Buttons', component: Buttons, exact: true },
  { path: '/buttons/buttons', name: 'Buttons', component: Buttons },
  { path: '/buttons/button-dropdowns', name: 'Dropdowns', component: ButtonDropdowns },
  { path: '/buttons/button-groups', name: 'Button Groups', component: ButtonGroups },
  { path: '/buttons/brand-buttons', name: 'Brand Buttons', component: BrandButtons },
  { path: '/charts', name: 'Charts', component: Charts },
  { path: '/icons', exact: true, name: 'Icons', component: CoreUIIcons },
  { path: '/icons/coreui-icons', name: 'CoreUI Icons', component: CoreUIIcons },
  { path: '/icons/flags', name: 'Flags', component: Flags },
  { path: '/icons/brands', name: 'Brands', component: Brands },
  { path: '/notifications', name: 'Notifications', component: Alerts, exact: true },
  { path: '/notifications/alerts', name: 'Alerts', component: Alerts },
  { path: '/notifications/badges', name: 'Badges', component: Badges },
  { path: '/notifications/modals', name: 'Modals', component: Modals },
  { path: '/notifications/toaster', name: 'Toaster', component: Toaster },
  { path: '/widgets', name: 'Widgets', component: Widgets },
  { path: '/users', exact: true,  name: 'Users', component: Users },
  { path: '/users/:id', exact: true, name: 'User Details', component: User }
];

export default routes;
