import axios from 'axios'
import setAuthToken from '../utils/setAuthToken'
import jwt_decode from 'jwt-decode'
import { SET_CURRENT_USER } from './types';
import {backend_address} from '../constant'

export const register = (userData, history) => dispatch => {
    axios.post(backend_address + '/api/auth/signup', userData)
        .then(res => {
            dispatch(setCurrentUser(null))
            history.push({
                pathname: '/',
                state: { isRegister: true }
            })
        })
        .catch(err => console.log(err))
}
export const loginUser = (userData, history) => dispatch => {
    var url = backend_address + "/api/auth/signin"
    axios.post(url, userData)
        .then(res => {
            localStorage.setItem('jwtToken', res.data.accessToken)
            localStorage.setItem('user', JSON.stringify(res.data));
            setAuthToken(res.data.accessToken)
            const decoded = jwt_decode(res.data.accessToken)
            dispatch(setCurrentUser(decoded))
            history.push('/dashboard')
        })
        .catch(
            err => {
                localStorage.removeItem('jwtToken')
                dispatch(setCurrentUser(null))
                history.push('/', { some: 'state' })
            })

}
export const setCurrentUser = decoded => {
    return {
        type: SET_CURRENT_USER,
        payload: decoded
    };
};