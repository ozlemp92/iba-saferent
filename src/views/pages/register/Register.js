import React from 'react'
import {
  CButton,
  CCard,
  CCardBody,
  CCardFooter,
  CCol,
  CContainer,
  CForm,
  CInput,
  CInputGroup,
  CInputGroupPrepend,
  CInputGroupText,
  CRow
} from '@coreui/react'
import CIcon from '@coreui/icons-react'
import PropTypes from 'prop-types'

import { register } from '../../../../src/actions/authActions';
import { connect } from 'react-redux';
import store from '../../../store.js';
class Register extends React.Component {
  constructor(props) {
      super(props);
      this.state = {
          username: '',
          email:'',
          password: '',
          repeatPassword:''
      };
      this.register = this.register.bind(this);
  }
  registerFunc(userRegisterData) {
    const requestOptions = {
        method: 'POST',
        headers: { 'Content-Type': 'application/json' },
        body: JSON.stringify({ userRegisterData })
    };
    
   this.props.register(userRegisterData,this.props.history)
}

register(e) {
  const { username, password,email,repeatPassword } = this.state;
  if(password != repeatPassword)
  {
    alert("şifreler birbiriyle uyuşmuyor!")
    return;
  }
  else if (username && email && password && repeatPassword) {
    const userRegisterData = {
      username: username,
      password:password,
      email:email,
      role:["user"]
    }
      this.registerFunc(userRegisterData);
  }
}

  render() {
    const { username, password, repeatPassword,email } = this.state;
    return (
    <div className="c-app c-default-layout flex-row align-items-center">
      <CContainer>
        <CRow className="justify-content-center">
          <CCol md="9" lg="7" xl="6">
            <CCard className="mx-4">
              <CCardBody className="p-4">
                <CForm>
                  <h1>Kayıt Ol</h1>
                  <p className="text-muted">Hesap Oluştun</p>
                  <CInputGroup className="mb-3">
                    <CInputGroupPrepend>
                      <CInputGroupText>
                        <CIcon name="cil-user" />
                      </CInputGroupText>
                    </CInputGroupPrepend>
                    <CInput type="text" onChange={(e) => { this.setState({ username: e.target.value }) }} placeholder="Username" autoComplete="username" />
                  </CInputGroup>
                  <CInputGroup className="mb-3">
                    <CInputGroupPrepend>
                      <CInputGroupText>@</CInputGroupText>
                    </CInputGroupPrepend>
                    <CInput type="text" onChange={(e) => { this.setState({ email: e.target.value }) }}placeholder="Email" autoComplete="email" />
                  </CInputGroup>
                  <CInputGroup className="mb-3">
                    <CInputGroupPrepend>
                      <CInputGroupText>
                        <CIcon name="cil-lock-locked" />
                      </CInputGroupText>
                    </CInputGroupPrepend>
                    <CInput type="password" onChange={(e) => { this.setState({ password: e.target.value }) }}placeholder="Password" autoComplete="new-password" />
                  </CInputGroup>
                  <CInputGroup className="mb-4">
                    <CInputGroupPrepend>
                      <CInputGroupText>
                        <CIcon name="cil-lock-locked" />
                      </CInputGroupText>
                    </CInputGroupPrepend>
                    <CInput type="password" onChange={(e) => { this.setState({ repeatPassword: e.target.value }) }} placeholder="Repeat password" autoComplete="new-password" />
                  </CInputGroup>
                  <CButton color="success"onClick={this.register} block>Hesap Oluştur</CButton>
                </CForm>
              </CCardBody>
              <CCardFooter className="p-4">
              </CCardFooter>
            </CCard>
          </CCol>
        </CRow>
      </CContainer>
    </div>
  )
}
}
const mapStateToProps = state => ({
  auth: state.auth,
})
export default connect(mapStateToProps, { register })(Register)
