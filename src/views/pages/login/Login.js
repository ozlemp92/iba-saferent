import React from 'react';
import axios from 'axios';
import { Link } from 'react-router-dom';
import PropTypes from 'prop-types'
import { connect } from 'react-redux';
import store from '../../../store.js';

import { loginUser } from '../../../../src/actions/authActions';
import {
  CButton,
  CCard,
  CCardBody,
  CCardGroup,
  CCol,
  CContainer,
  CForm,
  CInput,
  CInputGroup,
  CInputGroupPrepend,
  CInputGroupText,
  CRow, CAlert,CImg
} from '@coreui/react';
import Loading from '../../shared/loading';
import CIcon from '@coreui/icons-react'
import  rent from './../../../assets/icons/logo.png'
class Login extends React.Component {
  constructor(props) {
    super(props);
    this.logout();

    this.state = {
      username: '',
      password: '',
      submitted: false,
      isLogin: false,
      loading:false
    };

    this.handleChange = this.handleChange.bind(this);
    this.handleSubmit = this.handleSubmit.bind(this);

  }
  componentDidMount()
  {
    localStorage.setItem('jwtToken', "user")
  }
  login=(username, password)=> {

    const requestOptions = {
      method: 'POST',
      headers: { 'Content-Type': 'application/json' },
      body: JSON.stringify({ username, password })
    };
    localStorage.setItem('userName', JSON.stringify(username));
    const userData = {
      username: username,
      password: password
    }
    this.props.loginUser(userData, this.props.history)
    if (this.props.auth && !this.props.auth.isAuthenticated) {
      this.setState({ isLogin: true,loading:true });
      this.props.history("/dashboard")//servise sonra bağlicaz.
    }
    else  this.setState({ loading:false });
  }

  logout() {
    localStorage.removeItem('user');
  }
  handleChange(e) {
    const { name, value } = e.target;
    this.setState({ [name]: value });
  }
  handleSubmit(e) {
    this.setState({ loading:true });
    e.preventDefault();
    const { username, password } = this.state;
    if (username && password) {
      this.login(username, password);

    }
  }

  render() {
    return (
      <Loading loadingText={"Yükleniyor"} loading={this.state.loading}>
      {
      <div className="c-app c-default-layout flex-row align-items-center">
        <CContainer>
          <CRow className="justify-content-center">
            <CCol md="8">
              <CCardGroup>
                <CCard className="p-4">
                  <CCardBody>
                    <CForm style={{marginTop:"40px"}}>
                      <h1>Giriş</h1>
                      {this.props.location && this.props.location.state && this.props.location.state.isRegister &&
                        <CAlert color="success">
                          Kayıt başarılı lütfen giriş yapın!
              </CAlert>
                      }
                      {this.props.location && this.props.location.state && !this.props.location.state.isRegister &&
                        <p className="text-muted">Hesabınıza giriş yapın</p>
                      }
                      <CInputGroup className="mb-3">
                        <CInputGroupPrepend>
                          <CInputGroupText>
                            <CIcon name="cil-user" />
                          </CInputGroupText>
                        </CInputGroupPrepend>
                        <CInput type="text" value={this.state.username} onChange={(e) => { this.setState({ username: e.target.value }) }} placeholder="Kullanıcı Adı" autoComplete="username" />
                      </CInputGroup>
                      <CInputGroup className="mb-4">
                        <CInputGroupPrepend>
                          <CInputGroupText>
                            <CIcon name="cil-lock-locked" />
                          </CInputGroupText>
                        </CInputGroupPrepend>
                        <CInput type="password" value={this.state.password} onChange={(e) => { this.setState({ password: e.target.value }) }} placeholder="Şifre" autoComplete="current-password" />
                      </CInputGroup>
                      <CRow>
                        <CCol xs="6">
                          <CButton color="primary" onClick={this.handleSubmit} className="px-4">Giriş</CButton>
                        </CCol>
                        <CCol xs="6" className="text-right">
                          <CButton color="link" className="px-0">Şifremi Unuttum?</CButton>
                        </CCol>
                        <br />
                        <br />
                        {localStorage.getItem('jwtToken')== null &&
                          <CCol xs="12" className="text-right" style={{ color: "red" }} >
                            <h4>Kullanıcı Adı veya Şifre yanlış !</h4>
                          </CCol>
                        }
                      </CRow>
                    </CForm>
                  </CCardBody>
                </CCard>
                <CCard className="text-white bg-primary py-5 d-md-down-none" style={{ width: '44%' }}>
                  <CCardBody className="text-center">
                    <div>
                    <CImg src={rent} style={{width:"200px",margin:"-40px"}} to="/dashboard"  alt=""></CImg>
   
                      <h2 >Kaydol</h2>
                      <p>Hesabınız yok mu?</p>
                      <Link to="/register">
                        <CButton color="primary"
                        className="mt-3" active tabIndex={-1}>Hemen Kaydolun!</CButton>
                      </Link>
                    </div>
                  </CCardBody>
                </CCard>
              </CCardGroup>
            </CCol>
          </CRow>
        </CContainer>
      </div>
         }
         </Loading>
    )
  }
}
const mapStateToProps = state => ({
  auth: state.auth,
})
export default connect(mapStateToProps, { loginUser })(Login)
