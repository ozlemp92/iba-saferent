import React from 'react';
import { withRouter } from 'react-router-dom';
import {
  CBadge,
  CDataTable,
  CCard,
  CCardHeader,
  CCardBody,
  CButton,
  CCol,
  CRow
} from '@coreui/react'
import CIcon from '@coreui/icons-react'
import { Button, Icon } from 'semantic-ui-react'
import { getContractByUserId } from '../../repository/repo';

class ListContracts extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      showNewCustomerButton: true,
      dataCredit: [],
      contractData: [],
      customerJson: []

    }
  }
  componentDidMount() {
    var list = [];
    getContractByUserId(1).then(result => {
      debugger;
      console.log(result);

      this.setState({
        contractData: result
      });
      result.map((item, index) => {
        debugger;
        var json = {
          id: index,
          Kiracı: item.tenant.name + " " + item.tenant.surname,
          'Konut Sahibi': item.landlord.name + " " + item.landlord.surname,
          Durum: item.contractStatus === "STARTED" ? 'Aktif' : "Pasif",

          İşlemler: "",
        }
        debugger;
        list.push(json);
      });

      this.setState({
        customerJson: list
      });
    }).catch(err => { })
      .finally(err => { })
  }
  getBadge = Durum => {
    switch (Durum) {
      case 'Aktif': return 'success'
      case 'Inactive': return 'secondary'
      case 'Pending': return 'warning'
      case 'Pasif': return 'danger'
      default: return 'primary'
    }
  }

  redirectCreditOperation = () => {
    //this.props.history.push("/");
    this.props.history.push("/rent/creditOperations")
  }

  render() {

    const fields = ['Kiracı', 'Konut Sahibi', 'Durum', 'İşlemler']
    return (
      <>
        <CRow>
          <CCol xs="12" lg="12">
            <CCard>
              <CCardHeader>
                <Button icon primary circular style={{ float: 'right' }} onClick={this.redirectCreditOperation}>
                  <Icon name='plus' /> Yeni Kira Başvurusu
                </Button>
              </CCardHeader>
              <CCardBody>
                <CDataTable
                  items={this.state.customerJson}
                  fields={fields}
                  striped
                  itemsPerPage={5}
                  pagination
                  scopedSlots={{
                    'Durum':
                      (item) => (
                        <td>
                          <CBadge color={this.getBadge(item.Durum)}>
                            {item.Durum}
                          </CBadge>
                        </td>
                      )

                  }}
                />
              </CCardBody>
            </CCard>
          </CCol>
        </CRow>
      </>
    )
  }
}

export default withRouter(ListContracts);
