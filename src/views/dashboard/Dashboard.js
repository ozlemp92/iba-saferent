import React, { lazy } from 'react'
import {
  CBadge,
  CDataTable,
  CButton,
  CButtonGroup,
  CCard,
  CCardBody,
  CCardFooter,
  CCardHeader,
  CCol,
  CProgress,
  CRow,
  CCallout
} from '@coreui/react';
import 'semantic-ui-css/semantic.min.css';
import CIcon from '@coreui/icons-react'
import { Divider, Form, Label, Button, Icon, Checkbox, Card } from 'semantic-ui-react';
import MainChartExample from '../charts/MainChartExample.js'
import ListContracts from './ListContracts'
import { dataCreditJson } from '../jsonFile/listCustomer';

import { getContractByUserId } from '../../repository/repo';

const WidgetsDropdown = lazy(() => import('../widgets/WidgetsDropdown.js'))
const WidgetsBrand = lazy(() => import('../widgets/WidgetsBrand.js'))
class Dashboard extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      contractData: []
    }
  }


  render() {
    return (
      <>
        <WidgetsDropdown />
        <CRow>
          <CCol xs="12" lg="12">
            <ListContracts />
          </CCol>
        </CRow>
      </>
    )
  }
}

export default Dashboard;
