export const city = [
    {
        id: 1,
        name: "Bakü",
    },
    {
        id: 2,
        name: "Nahçivan"
    },
    {
        id: 3,
        name: "Şeki"
    },
    {
        id: 4,
        name: "Gence"
    },
    {
        id: 5,
        name: "Mingeçevir"
    },
]
export const county = [
    {
        id: 1,
        cityId: 1,
        name: "Binegedi"
    },
    {
        id: 2,
        cityId: 1,
        name: "Karadağ"
    },
    {
        id: 3,
        cityId: 1,
        name: "Hazar"
    },
    {
        id: 4,
        cityId: 1,
        name: "Sebail"
    },
    {
        id: 5,
        cityId: 1,
        name: "Sabuncu"
    },
    {
        id: 6,
        cityId: 1,
        name: "Surahanı"
    },
    {
        id: 7,
        cityId: 1,
        name: "Hatai"
    },
    {
        id: 8,
        cityId: 1,
        name: "Yasamal"
    },
    {
        id: 9,
        cityId: 2,
        name: "Babek"
    },
    {
        id: 10,
        cityId: 2,
        name: "Culfa"
    },
    {
        id: 11,
        cityId: 2,
        name: "Kıvrak"
    },
    {
        id: 12,
        cityId: 2,
        name: "Ordubad"
    },
    {
        id: 13,
        cityId: 2,
        name: "Şahbuz"
    },
    {
        id: 14,
        cityId: 2,
        name: "Şerur"
    }
]

export const dataCreditJson = [
    {
        id: 1,
        Kiracı: "Ali Uzunsoy",
        'Konut Sahibi': "Özlem Pala",
        Durum: "Aktif",
        İşlemler: "",
    },
    {
        id: 2,
        Kiracı: "Ahmet Varlı",
        'Konut Sahibi': "Yaşar Yaşa",
        Durum: "Aktif",
        İşlemler: "",
    },
    {
        id: 3,
        Kiracı: "Ayşe Gündüz",
        'Konut Sahibi': "Çidem Sarı",
        Durum: "Aktif",
        İşlemler: "",
    },
    {
        id: 4,
        Kiracı: "Fatma Uysal",
        'Konut Sahibi': "Mahmut Onurlu",
        Durum: "Pasif",
        İşlemler: "",
    },
    {
        id: 5,
        Kiracı: "Nükhet Durmuş",
        'Konut Sahibi': "Esengül Korkmaz",
        Durum: "Aktif",
        İşlemler: "",
    },
];