import React from 'react';
import LoadingOverlay from 'react-loading-overlay';
import BounceLoader from 'react-spinners/ScaleLoader';

const Loading = ({children, loading, loadingText}) => {
    return (

            <LoadingOverlay
                        active={loading}
                        spinner={<BounceLoader />}
                        text={loadingText}
                    >
            {children}
            </LoadingOverlay>
    )
}

export default Loading;