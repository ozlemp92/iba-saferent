import React from 'react'
import { Button, Header, Icon } from 'semantic-ui-react';
import ReactDOM from 'react-dom';
import CIcon from '@coreui/icons-react';
import 'react-responsive-modal/styles.css';
import '../../../css/deletepopup.css'
import { Modal } from 'react-responsive-modal';
class deletePopup extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
    }


  }
  render() {
    return (

      <>
        {this.props.onOpenModal &&
          <div>
            <Modal onOpenModal={this.props.onOpenModal} open={this.props.open} onClose={this.props.onCloseModal}
              center>
              <br />
              <div className="modal-body">Bu müşteriyi silmek istiyor musunuz!</div>
              <div className="modal-footer">
                <button type="button" onClick={this.props.onCloseModal} className="btn btn-secondary">Evet</button>
                <button type="button" style={{backgroundColor:"#2185d0"}} onClick={this.props.onCloseModal} className="btn btn-primary">Hayır</button></div>
            </Modal>
          </div>
        }
      </>

    )
  }
}

export default deletePopup
