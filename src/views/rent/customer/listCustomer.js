import React, { useState, useEffect } from 'react';
import DeletePopup from "./deletePopup";
import { withRouter } from 'react-router-dom';
import {
  CBadge,
  CDataTable,
  CButton,
  CCard,
  CCardBody,
  CCardFooter,
  CCardHeader,
  CCol,
  CCollapse,
  CDropdownItem,
  CDropdownMenu,
  CDropdownToggle,
  CFade,
  CForm,
  CFormGroup,
  CFormText,
  CValidFeedback,
  CInvalidFeedback,
  CTextarea,
  CInput,
  CInputFile,
  CInputCheckbox,
  CInputRadio,
  CInputGroup,
  CInputGroupAppend,
  CInputGroupPrepend,
  CDropdown,
  CInputGroupText,
  CLabel,
  CSelect,
  CRow
} from '@coreui/react'
import CIcon from '@coreui/icons-react'
import { Divider, Form, Label, Button, Icon, Modal, Header, Checkbox, Table, Menu } from 'semantic-ui-react'
import 'semantic-ui-css/semantic.min.css';
import { getContractByUserId } from '../../../../src/repository/repo';
import { dataCreditJson } from '../../jsonFile/listCustomer';
import { getAllCredit } from '../../../../src/repository/repo';
class listCustomer extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      showDeletePopup: false,
      open: false,
      showNewCustomerButton: true,
      dataCredit: [],
      contractData: [],
      customerJson: []

    }
    this.redirectCreateCustomer = this.redirectCreateCustomer.bind(this);
    this.deleteConfirm = this.deleteConfirm.bind(this);
  }
  componentDidMount() {
    var list = [];
    getContractByUserId(1).then(result => {
      debugger;
      console.log(result);

      this.setState({
        contractData: result
      });
      result.map((item, index) => {
        debugger;
        var json = {
          id: index,
          Kiracı: item.tenant.name + " " + item.tenant.surname,
          'Konut Sahibi': item.landlord.name + " " + item.landlord.surname,
          Durum: item.contractStatus ==="STARTED" ?  'Aktif':"Pasif",
                    
          İşlemler: "",
        }
        debugger;
        list.push(json);
      });
     
      this.setState({
        customerJson: list
      });
    }).catch(err => { })
      .finally(err => { })
  }
  onOpenModal = () => {
    this.setState({ open: true });
  };
   getBadge = Durum => {
    switch (Durum) {
      case 'Aktif': return 'success'
      case 'Inactive': return 'secondary'
      case 'Pending': return 'warning'
      case 'Pasif': return 'danger'
      default: return 'primary'
    }
  }
  onCloseModal = () => {
    this.setState({ open: false });
  };

  redirectCreateCustomer(id) {
    var data = dataCreditJson.filter(x => x.id === id);
    this.props.history.push("/");
    this.props.history.push({
      pathname: '/rent/customer/createCustomer',
      state: {
        data: data[0]
      }
    })
  }
  deleteConfirm() {
    this.setState({
      showDeletePopup: !this.state.showDeletePopup
    });
  }
  render() {

  
    const fields = ['Kiracı', 'Konut Sahibi', 'Durum', 'İşlemler']
    return (
      <>

        <CRow>
          <CCol xs="12" lg="12">
            <CCard>
              <CCardHeader>
                {this.state.open && (
                  <DeletePopup
                    open={this.state.open}
                    onOpenModal={this.onOpenModal}
                    onCloseModal={this.onCloseModal} />
                )}
              </CCardHeader>
              <CCardBody>
                <CDataTable
                  items={this.state.customerJson}
                  fields={fields}
                  itemsPerPage={5}
                  pagination
                  scopedSlots = {{
                    'Durum':
                      (item)=>(
                        <td>
                          <CBadge color={this.getBadge(item.Durum)}>
                            {item.Durum}
                          </CBadge>
                        </td>
                      )
    
                  }}
                />
              </CCardBody>
            </CCard>
          </CCol>
        </CRow>

      </>
    )
  }
}

export default withRouter(listCustomer);
