import React from 'react'
import {
  CButton,
  CCard,
  CCardBody,
  CCardFooter,
  CCardHeader,
  CCol,
  CCollapse,
  CDropdownItem,
  CDropdownMenu,
  CDropdownToggle,
  CFade,
  CForm,
  CFormGroup,
  CFormText,
  CValidFeedback,
  CInvalidFeedback,
  CTextarea,
  CInput,
  CInputFile,
  CInputCheckbox,
  CInputRadio,
  CInputGroup,
  CInputGroupAppend,
  CInputGroupPrepend,
  CDropdown,
  CInputGroupText,
  CLabel,
  CSelect,
  CRow
} from '@coreui/react'
import CIcon from '@coreui/icons-react'
import { Divider, Form, Label, Button, Icon, Checkbox, Table, Menu } from 'semantic-ui-react'
import 'semantic-ui-css/semantic.min.css';
class createCustomer extends React.Component {
  constructor(props) {
    super(props);
    this.state = {

    }

  }
  redirectCreateCustomer=()=>
  {

  }
  componentDidMount()
  {
  }
  redirectListCustomer = (id) => {
    // this.props.history.push(`./customer/createCustomer/${customer.id}`);
    this.props.history.push("/");
    this.props.history.push("/rent/customer/listCustomer")
  }
  render() {

    return (
      <>
        <CCard>
          <CCardHeader>
            <Label style={{ background: "white", fontWeight: "600", fontSize: "15px" }} >Aşağıdaki formu doğru ve eksiksiz doldurun</Label>
          </CCardHeader>
          <CCardBody>
            <CForm action="" method="post" className="form-horizontal">
              <CFormGroup row>
                <CCol md="8" style={{ borderRight: "1px solid #e7eaec" }}>
                  <CFormGroup row>
                    <CCol md="3">

                      <Label htmlFor="name" style={{ background: "white", fontWeight: "600", fontSize: "13px" }}>Adı</Label>
                    </CCol>
                    <CCol xs="10" md="8">
                      <CInput type="text" value={this.props.location && this.props.location.state && this.props.location.state.data
                      && this.props.location.state.data.name}
                       style={{ background: "white", fontWeight: "600", fontSize: "13px" }} 
                       id="name" name="name" placeholder="Adı..." />
                    </CCol>
                  </CFormGroup>
                  <CFormGroup row>
                    <CCol md="3">

                      <Label htmlFor="surname" style={{ background: "white", fontWeight: "600", fontSize: "13px" }}>Soy Adı</Label>
                    </CCol>
                    <CCol xs="10" md="8">
                      <CInput type="text"value={this.props.location && this.props.location.state && this.props.location.state.data
                      && this.props.location.state.data.surname}  style={{ background: "white", fontWeight: "600", fontSize: "13px" }} id="surname" 
                      name="surname" placeholder="Soy Adı..." />
                    </CCol>
                  </CFormGroup>
                  <CFormGroup row>
                    <CCol md="3">

                      <Label htmlFor="identityNumber" style={{ background: "white", fontWeight: "600", fontSize: "13px" }}>Kimlik Numarası</Label>
                    </CCol>
                    <CCol xs="10" md="8">
                      <CInput value={this.props.location && this.props.location.state && this.props.location.state.data
                      && this.props.location.state.data.identity} type="text" style={{ background: "white", fontWeight: "600", fontSize: "13px" }} id="identityNumber" name="identityNumber" placeholder="Kimlik Numarası..." />
                    </CCol>
                  </CFormGroup>
                  <CFormGroup row>
                    <CCol md="3">
                      <Label  style={{ background: "white", fontWeight: "600", fontSize: "13px" }} htmlFor="mobileNumber">Cep Telefonu</Label>
                    </CCol>
                    <CCol xs="10" md="8">
                      <CInput type="text" value={this.props.location && this.props.location.state && this.props.location.state.data
                      && this.props.location.state.data.phone} id="mobileNumber" name="mobileNumber" style={{ background: "white", fontWeight: "600", fontSize: "13px" }} placeholder="Cep Telefonu..." />
                      {/* <CFormText className="help-block">Please enter your password</CFormText> */}
                    </CCol>
                  </CFormGroup>
                  <CFormGroup row>
                    <CCol md="3">
                      <Label htmlFor="address" style={{ background: "white", fontWeight: "600", fontSize: "13px" }}>Adres</Label>
                    </CCol>
                    <CCol xs="10" md="8">
                      <CInput value={this.props.location && this.props.location.state && this.props.location.state.data
                      && this.props.location.state.data.address} type="text" style={{ background: "white", fontWeight: "600", fontSize: "13px" }} id="address" name="address" placeholder="Adres..." />
                    </CCol>
                  </CFormGroup>
                </CCol>
              </CFormGroup>
            </CForm>
          </CCardBody>
          <CCardFooter>        
            <Button content='Kaydet' onClick={this.redirectCreateCustomer} primary  type="button" />
            <Button onClick={this.redirectListCustomer} content='Geri' negative  type="button" />
          </CCardFooter>
        </CCard>

      </>
    )
  }
}

export default createCustomer
