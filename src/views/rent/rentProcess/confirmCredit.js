import React from 'react';
import { Field, reduxForm } from 'redux-form';
import { Divider, Form, Label, Button, Icon, Dropdown } from 'semantic-ui-react';
import {
    CButton,
    CCard,
    CCardBody,
    CCardFooter,
    CCardHeader,
    CCol,
    CCollapse,
    CDropdownItem,
    CDropdownMenu,
    CDropdownToggle,
    CDropdownHeader,
    CFade,
    CForm,
    CFormGroup,
    CFormText,
    CValidFeedback,
    CInvalidFeedback,
    CTextarea,
    CInput,
    CInputFile,
    CInputCheckbox,
    CInputRadio,
    CInputGroup,
    CInputGroupAppend,
    CInputGroupPrepend,
    CDropdown,
    CInputGroupText,
    CLabel,
    CSelect,
    CRow, CAlert
} from '@coreui/react';
import Loading from '../../shared/loading';
import CIcon from '@coreui/icons-react'
import { withRouter } from "react-router-dom"
import StepWizard from 'react-step-wizard';
import tick from './../../../assets/icons/tick.svg';
class confirmCredit extends React.Component {
    constructor(props) {
        super(props);
        const creditData = JSON.parse(localStorage.getItem('credit'));
        this.state = {
            identityNumber : creditData.applicantIdentityNumber,
            name: creditData.applicantName,
            surname: creditData.applicantSurname,
            loading:true
        }
        // this.redirectCreateCustomer = this.redirectCreateCustomer.bind(this);
        // this.deleteConfirm = this.deleteConfirm.bind(this);
    }
componentDidMount()
{
    setTimeout(
        () => this.setState({ loading: false }),
        1000
    );
}
    sendToDashboard = () => {
        this.setState({ loading: true })
        this.props.history.push("/dashboard");
    }
    render() {
        return (
            <Loading loadingText={"Yükleniyor"} loading={this.state.loading}>
        {
            <>
                <CCardBody style={{ width: "50%", marginLeft: "25%" }}>
                    <CAlert color="success" style={{ border: "5px solid #2eb85c" }}>
                        <h4 style={{ textAlign: "center" }} className="alert-heading">BAŞARILI!</h4>
                        <p style={{ textAlign: "center" }}>
                            Kimlik No: {this.state.identityNumber}  için
                            </p>
                        <p style={{ textAlign: "center" }}>
                            Ad: {this.state.name}  adına kira kredisi süreci tamamlanmıştır.
                </p>
                        <p style={{ textAlign: "center" }}>   <img src={tick} alt="" width="50" height="50"></img></p>
                        <p style={{ textAlign: "center" }} className="mb-0">
                            Onaylanma Talebi Başarıyla iletildi!
                </p>
                        <br />
                        <p style={{ textAlign: "center" }} className="mb-0">
                            <CButton color="success" type="button" onClick={this.sendToDashboard}>BAŞVURUYU TAMAMLA!</CButton>{' '}

                        </p>

                    </CAlert>
                </CCardBody>

            </>
                  }
                  </Loading>
        )
    }
}

export default withRouter(confirmCredit);
