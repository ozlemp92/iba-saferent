import React, { useEffect, useState, useRef } from 'react'
import { Field, reduxForm } from 'redux-form';
import { Divider, Form, Label, Button, Icon } from 'semantic-ui-react';
import 'semantic-ui-css/semantic.min.css';
import {
  CButton,
  CCard,
  CCardBody,
  CCardFooter,
  CCardHeader,
  CCol,
  CForm,
  CFormGroup,
  CInput,
  CInputCheckbox,
  CLabel,
  CRow
} from '@coreui/react';
import Loading from '../../shared/loading';
import redo from './../../../assets/icons/redo.svg'
import CIcon from '@coreui/icons-react'
import { useHistory } from "react-router";
import StepWizard from 'react-step-wizard';
import { withRouter } from 'react-router-dom';
// const [loading, setLoading] = useState(true)
class WizardFormFirst extends React.Component {

  constructor(props) {
    super(props);
    this.state = {
      loading: false
    }
    this.handleSubmit = this.handleSubmit.bind(this);
  }

  // const history = useHistory();

  // const [isFirstLoading, setIsFirstLoading] = useState(true);
  // const [updateDetector, toggleUpdate] = useState(true);

  // useEffect(() => {
  //   debugger;
  //   if (isFirstLoading) {
  //     const width = window.innerWidth;
  //     const isDisplay = width >836 ? true: false;
  //     setIsFirstLoading(false);
  //     setDisplay(isDisplay)
  //     return;
  //   }


  // },[updateDetector])
  openLoading = () => {
    this.setState({ loading: true })
  }

  handleSubmit = (event) => {
    debugger;
    setTimeout(() => {
      this.openLoading()
    }, 1000);
 
    event.preventDefault();
    const data = new FormData(event.target)
    var dataSave = {
      page: 1,
      applicantIdentityNumber: event.target["identityNumber"].value,
      applicationPhoneNumber: event.target["mobileNumber"].value
    }
    this.state.loading = true
    this.setState(
      { loading: true  },
      this.props.onSubmit(dataSave)
    );
   debugger;

  }
  //   style={{
  //     display: display  ? "inherit": "none"

  // }}
  // handleSubmit = (event) => {
  //     // Prevent default behavior
  //     event.preventDefault();

  //     const data = new FormData(event.target);
  //     history.push('/rent/rentProcess/wizardFormSecond', { some: 'state' })
  //     // Access FormData fields with `data.get(fieldName)`
  //     // For example, converting to upper case
  //     // data.set('username', data.get('username').toUpperCase());

  //     // Do your Axios stuff here
  // };
  // handleSubmit = (event) => {
  //   event.preventDefault()

  // }
  render() {
    return (
      <Loading loadingText={"Yükleniyor"} loading={this.state.loading}>
        {
          <CCard>
            <CCardHeader>
              <Label style={{ background: "white", fontWeight: "600", fontSize: "15px" }} >Aşağıdaki formu doğru ve eksiksiz doldurun</Label>
            </CCardHeader>
            <CCardBody>
              <CForm onSubmit={this.handleSubmit} method="post" className="form-horizontal" style={{ padding: "20px 0 0 0" }}>
                <CFormGroup row>
                  <CCol md="8" style={{ borderRight: "1px solid #e7eaec" }}>
                    <CFormGroup row>
                      <CCol md="3">
                        <Label htmlFor="identityNumber" style={{ background: "white", fontWeight: "600", fontSize: "13px" }}>Kimlik Numarası</Label>
                      </CCol>
                      <CCol xs="10" md="8">
                        <CInput type="text" id="identityNumberid" name="identityNumber" placeholder="Kimlik Numarası" />
                        {/* <CFormText className="help-block">Please enter your email</CFormText> */}
                      </CCol>
                    </CFormGroup>
                    <CFormGroup row>
                      <CCol md="3">
                        <Label style={{ background: "white", fontWeight: "600", fontSize: "13px" }} htmlFor="mobileNumber">Cep Telefonu</Label>
                      </CCol>
                      <CCol xs="10" md="8">
                        <CInput type="text" id="mobileNumberid" name="mobileNumber" placeholder="Cep Telefonu" />
                        {/* <CFormText className="help-block">Please enter your password</CFormText> */}
                      </CCol>
                    </CFormGroup>
                    <CFormGroup row style={{ marginTop: "40px" }}>
                      <CCol md="11">
                        <Label style={{ width: "100%" }}><CIcon name="cil-ban" style={{ marginRight: "2px" }} />Kişisel Verilerin İşlenmesi ve Aktarılmasına İlişkin Aydınlatma Metni'ni görüntülemek için tıklayınız. (Onaylamadan önce görüntülemeniz gerekmektedir.)</Label>
                        <CFormGroup variant="checkbox" className="checkbox" style={{ color: "#fff", backgroundColor: "#337ab7", padding: "5px 0px 5px 25px" }}>
                          <CInputCheckbox required
                            id="checkbox1"
                            name="checkbox1"
                            value="option1"
                          />
                          <CLabel variant="checkbox" className="form-check-label" htmlFor="checkbox1">Kişisel Verilerin İşlenmesi ve Aktarılmasına İlişkin Aydınlatma Metni'ni okudum.</CLabel>
                        </CFormGroup>
                      </CCol>
                    </CFormGroup>
                    <CFormGroup row>
                      <CCol md="3"></CCol>
                      <CCol md="8">
                        <Button className="float-right" primary type="submit" >Devam</Button>
                      </CCol>
                    </CFormGroup>
                  </CCol>
                  <CRow  className="d-none d-md-block">
                    <CCol md="12">
                      <span style={{ marginTop: "5px", marginLeft: "40px", background: "white", fontWeight: "600", fontSize: "15px" }}>Lütfen formu doldurup ilerletin.</span>
                    </CCol>
                  </CRow>
                  <CRow>
                    <CCol md="8" className="d-none d-md-block">
                      <img style={{ marginTop: "20px", marginLeft: "-100px" }} src={redo} alt="" width="100" height="100"></img>
                    </CCol>
                  </CRow>
                </CFormGroup>
              </CForm>
            </CCardBody>
            <CCardFooter>
              {/* <CButton type="reset" size="sm" color="danger"><CIcon name="cil-ban" /> İptal</CButton> */}
            </CCardFooter>
          </CCard>

        }
      </Loading>

    )
  }
}
export default withRouter(reduxForm({
  WizardFormFirst: WizardFormFirst,
  value: 1333,
  form: 'fsfggdgd', // <------ same form name
  destroyOnUnmount: false, // <------ preserve form data
  forceUnregisterOnUnmount: true // <------ unregister fields on unmoun
})(WizardFormFirst))


