import React from 'react';
import { Field, reduxForm } from 'redux-form';
import { Divider, Form, Label, Button, Icon, Dropdown } from 'semantic-ui-react';
import { createHashHistory } from 'history'
import { withRouter } from 'react-router-dom'
import {
    CCard,
    CCardBody,
    CCardFooter,
    CCardHeader,
    CCol,
    CForm,
    CFormGroup,
    CInput,
    CLabel,
    CSelect,
    CRow
} from '@coreui/react';
import Loading from '../../shared/loading';
import { saveCredit } from '../../../../src/repository/repo';
import { educationTypes, occupationTypes } from '../../../../src/repository/repo';
class wizardFormSecond extends React.Component {
    constructor(props) {

        super(props);
        this.state = {
            name: null,
            surname: null,
            identityNumber: null,
            educationStatus: null,
            job: null,
            monthlyRentPrice: null,
            monthlySalary: null,
            creditTerm: null,
            occupationTypes: [],
            educationTypes: [],
            loading: false
        }
        this.handleSubmit = this.handleSubmit.bind(this);
    }

    componentDidMount() {
        occupationTypes().then(result => {
            this.setState({ occupationTypes: result })
        }).catch(err => {

        })
        educationTypes().then(res => {
            this.setState({ educationTypes: res })
        }).catch(err => {

        })
    }

    saveCredit(dataSave) {
        saveCredit(dataSave).then(data => {
            localStorage.setItem('credit', JSON.stringify(data));
            this.props.onSubmit();
        }).catch(error => {this.setState({ loading: false })
        }).finally(this.setState({ loading: false }))
    }
    
    handleSubmit = (event) => {
        debugger;
        setTimeout(
            () => this.setState({ loading: true }), 
            3000
          );
        event.preventDefault();
        var dataSave = {
            applicantIdentityNumber: event.target["identityNumber"].value,
            applicantName: event.target["customerName"].value,
            applicantSurname: event.target["surname"].value,
            applicationPhoneNumber: event.target["phoneNumber"].value,
            educationType: event.target["educationType"].value,
            monthlyRentPrice: event.target["monthlyRentPrice"].value,
            monthlySalary: event.target["monthlySalary"].value,
            occupationType: event.target["occupationType"].value,
            rentPeriod: event.target["rentPeriod"].value,
            status: "RECEIVED",
            user:{
                "id": JSON.parse(localStorage.getItem('user')) !== null ?JSON.parse(localStorage.getItem('user')).id : 0
            }
        }
        this.setState(
            { loading: true  },
            this.saveCredit(dataSave)
          );
        //localStorage.setItem('credit', JSON.stringify(dataSave));
       
    }

    render() {
        const { previousPage, idNumber, phoneNumber } = this.props

        return (
            <>
             <Loading loadingText={"Yükleniyor"} loading={this.state.loading}>
        {
                <CForm className="form-horizontal" onSubmit={this.handleSubmit}>
                    <CInput id="phoneNumber" value={phoneNumber} name="phoneNumber" type="hidden" />
                    <CRow>
                        <CCol xs="12" md="12">
                            <CCard >
                                <CCardHeader style={{ color: "blue" }}>
                                    <Label style={{ background: "white", fontWeight: "600", fontSize: "13px", color: "blue" }}>KİMLİK BİLGİLERİ</Label>
                                </CCardHeader>
                                <CCardBody>
                                    <CFormGroup row>
                                        <CCol xs="12" md="6" style={{ borderRight: "1px solid #e7eaec", paddingBottom: "15px" }}>
                                            <CLabel style={{ background: "white", fontWeight: "600", fontSize: "13px", }} htmlFor="name">Adı</CLabel>
                                            <CInput id="customerName" value={this.state.name} name="customerName" onChange={(e) => { this.setState({ name: e.target.value }) }} required />
                                        </CCol>
                                        <CCol xs="12" md="6">
                                            <CLabel style={{ background: "white", fontWeight: "600", fontSize: "13px", }} htmlFor="surname">Soyadı</CLabel>
                                            <CInput id="surname" value={this.state.surname} onChange={(e) => { this.setState({ surname: e.target.value }) }} name="surname" required />
                                        </CCol>
                                    </CFormGroup>
                                    <CFormGroup row>
                                        <CCol xs="12" md="12">
                                            <CLabel style={{ background: "white", fontWeight: "600", fontSize: "13px", }} onChange={(e) => { this.setState({ identityNumber: e.target.value }) }} htmlFor="identityNumber">Kimlik Numarası</CLabel>
                                            <CInput id="identityNumber" disabled={true} value={idNumber} name="identityNumber" required />
                                        </CCol>
                                    </CFormGroup>
                                </CCardBody>
                            </CCard>
                        </CCol>
                    </CRow>
                    <CRow>
                        <CCol xs="12" md="12">
                            <CCard >
                                <CCardHeader style={{ color: "blue" }}>
                                    <Label style={{ background: "white", fontWeight: "600", fontSize: "13px", color: "blue" }}>KİŞİSEL BİLGİLER</Label>
                                </CCardHeader>
                                <CCardBody>
                                    <CFormGroup row>
                                        <CCol xs="12" md="6" style={{ borderRight: "1px solid #e7eaec", paddingBottom: "15px" }}>
                                            <CLabel style={{ background: "white", fontWeight: "600", fontSize: "13px", }} htmlFor="occupationType">Meslek</CLabel>


                                            <CSelect onChange={(e) => { this.setState({ occupationType: e.target.value }) }}
                                                custom name="occupationType" id="occupationType">
                                                {this.state.occupationTypes && this.state.occupationTypes.map(fbb =>
                                                    <option key={fbb} value={fbb.key}>{fbb}</option>)};
                                            </CSelect>
                                        </CCol>
                                        <CCol xs="12" md="6">
                                            <CLabel style={{ background: "white", fontWeight: "600", fontSize: "13px", }} htmlFor="profession">Öğrenim Durumu</CLabel>
                                            <CSelect custom name="educationType" onChange={(e) => { this.setState({ educationType: e.target.value }) }} id="educationType">
                                                {this.state.educationTypes && this.state.educationTypes.map(fbb =>
                                                    <option key={fbb} value={fbb.key}>{fbb}</option>)};

                                            </CSelect>
                                        </CCol>
                                    </CFormGroup>
                                    <CFormGroup row>
                                        <CCol xs="12" md="12">
                                            <CLabel style={{ background: "white", fontWeight: "600", fontSize: "13px", }} onChange={(e) => { this.setState({ monthlySalary: e.target.value }) }} htmlFor="income">Aylık Net Gelir</CLabel>
                                            <CInput id="monthlySalary" name="monthlySalary" required />
                                        </CCol>
                                    </CFormGroup>
                                </CCardBody>
                            </CCard>

                        </CCol>
                    </CRow>
                    <CRow>
                        <CCol xs="12" md="12">
                            <CCard >
                                <CCardHeader style={{ color: "blue" }}>
                                    <Label style={{ background: "white", fontWeight: "600", fontSize: "13px", color: "blue" }}>KREDİ BAŞVURU BİLGİLERİ</Label>
                                </CCardHeader>
                                <CCardBody>
                                    <CFormGroup row>
                                        <CCol xs="12" md="6" style={{ borderRight: "1px solid #e7eaec", paddingBottom: "15px" }}>
                                            <CLabel style={{ background: "white", fontWeight: "600", fontSize: "13px", }} onChange={(e) => { this.setState({ monthlyRentPrice: e.target.value }) }} htmlFor="creditAmount">Kira Tutarı (Aylık)</CLabel>
                                            <CInput id="monthlyRentPrice" name="monthlyRentPrice" required />
                                        </CCol>
                                        <CCol xs="12" md="6">
                                            <CLabel style={{ background: "white", fontWeight: "600", fontSize: "13px", }} onChange={(e) => { this.setState({ rentPeriod: e.target.value }) }} htmlFor="creditTerm">Kredi Vadesi (Ay)</CLabel>
                                            <CInput id="rentPeriod" name="rentPeriod" required />
                                        </CCol>
                                    </CFormGroup>
                                </CCardBody>
                                <CCardFooter>
                                    <div className="float-right">
                                        <div>
                                            <Button content='İptal' negative type="button" onClick={previousPage} />
                                            <Button content='Kaydet' primary type="submit" />
                                        </div>
                                    </div>
                                </CCardFooter>
                            </CCard>

                        </CCol>
                    </CRow>
                </CForm>
                    }
                    </Loading>
            </>
        )
    }
}
export default withRouter(reduxForm({
    form: 'wizard', // <------ same form name
    destroyOnUnmount: false, // <------ preserve form data
    forceUnregisterOnUnmount: true // <------ unregister fields on unmoun
})(wizardFormSecond));
