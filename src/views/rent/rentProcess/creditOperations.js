import React, { Component } from 'react'
import PropTypes from 'prop-types'
import WizardFormFirst from './wizardFormFirst'
import WizardFormSecond from './wizardFormSecond';
import ConfirmCreditList from './confirmCreditList';
import { Divider, Form, Label } from 'semantic-ui-react';
import Loading from '../../shared/loading';
import 'semantic-ui-css/semantic.min.css';
import {
  CBadge
} from '@coreui/react'
class WizardForm extends Component {
  constructor(props) {
    super(props)
    this.nextPage = this.nextPage.bind(this)
    this.previousPage = this.previousPage.bind(this)
    this.state = {
      page: 1,
      applicantIdentityNumber: null,
      applicationPhoneNumber: null,
      loading:false
    }
  }

  nextPage(data) {
    if (data != undefined && data.page === 1) {
      this.setState({
        applicantIdentityNumber: data.applicantIdentityNumber,
        applicationPhoneNumber: data.applicationPhoneNumber,
        loading:true
      })
    }
    setTimeout(
      () => this.setState({ loading: false }), 
      1000
    );
    this.setState({ page: this.state.page + 1 })
  }

  previousPage() {
    this.setState({ page: this.state.page - 1,loading:true })
  }
  handleSubmit = values => {
    const { children, onSubmit } = this.props;
    const { page } = this.state;
     const isLastPage = page === React.Children.count(children) - 1;
    if (isLastPage) {
      this.setState({ loading:false })
      return onSubmit(values);
    } else {
      this.setState({ loading:false })
      this.next(values);
    }
  };

  render() {
    const { page } = this.state;
    return (
      <Loading loadingText={"Yükleniyor"} loading={this.state.loading}>
      {
      <div>
        {page === 1 && <>
          <Label style={{ color: "white", background: "#1a7bb9", fontWeight: "600", fontSize: "13px", width: "25%", padding: "10px", borderRadius: "1rem" }}>1. Kişisel Bilgiler</Label>
          <Label style={{ color: "#aaa", background: "#d9edf7", fontWeight: "600", fontSize: "13px", width: "25%", padding: "10px", borderRadius: "1rem" }}>2. Kredi Talebi</Label>
        </>}
        {page === 2 &&
          <>
            <Label style={{ color: "#fff", background: "#6fd1bd", fontWeight: "600", fontSize: "13px", width: "25%", padding: "10px", borderRadius: "1rem" }}>1. Kişisel Bilgiler</Label>
            <Label style={{ color: "white", background: "#1a7bb9", fontWeight: "600", fontSize: "13px", width: "25%", padding: "10px", borderRadius: "1rem" }}>2. Kredi Talebi</Label>
          </>}
        <br /><br />
        {page === 1 && <WizardFormFirst onSubmit={this.nextPage} />}
        {page === 2 && (
          <WizardFormSecond
            previousPage={this.previousPage}
            onSubmit={this.nextPage}
            idNumber={this.state.applicantIdentityNumber}
            phoneNumber={this.state.applicationPhoneNumber}
          />
        )}
        {page === 3 && <ConfirmCreditList creditData={JSON.parse(localStorage.getItem('credit'))}/>}
      </div>
              }
              </Loading>
    )
  }
}

export default WizardForm