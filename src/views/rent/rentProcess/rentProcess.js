import React from 'react'
import {
  CButton,
  CCard,
  CCardBody,
  CCardFooter,
  CCardHeader,
  CCol,
  CCollapse,
  CDropdownItem,
  CDropdownMenu,
  CDropdownToggle,
  CFade,
  CForm,
  CFormGroup,
  CFormText,
  CValidFeedback,
  CInvalidFeedback,
  CTextarea,
  CInput,
  CInputFile,
  CInputCheckbox,
  CInputRadio,
  CInputGroup,
  CInputGroupAppend,
  CInputGroupPrepend,
  CDropdown,
  CInputGroupText,
  CLabel,
  CSelect,
  CRow
} from '@coreui/react'
import StepWizard from 'react-step-wizard';
const RentProcess = () => {
  return (
      
    <CCard>
    <CCardHeader>
      Aşağıdaki formu doğru ve eksiksiz doldurun!
    </CCardHeader>
    <CCardBody>
      <CForm action="" method="post">
      <CFormGroup row>
          <CLabel sm="5" col="lg" htmlFor="input-large">Tc Kimlik Numarası</CLabel>
          <CCol sm="6">
            <CInput size="lg" type="text" id="input-large" name="input-large" className="input-lg" />
          </CCol>
        </CFormGroup>
        <CFormGroup row>
          <CLabel sm="5" col="lg" htmlFor="input-large">Cep Telefonu</CLabel>
          <CCol sm="6">
            <CInput size="lg" type="text" id="input-large" name="input-large" className="input-lg"/>
          </CCol>
        </CFormGroup>
      </CForm>
    </CCardBody>
    <CCardFooter>
      <CButton type="submit" size="lg" color="primary">Başvur</CButton>
      <CButton type="reset" size="lg" color="secondary">Geri</CButton>
    </CCardFooter>
  </CCard>

  )
}

export default RentProcess
