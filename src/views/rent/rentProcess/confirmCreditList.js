import React from 'react';
import {
    CButton,
    CCard,
    CCardBody,
    CCardHeader,
    CCol,
    CForm,
    CRow
} from '@coreui/react';
import Loading from '../../shared/loading';
import { withRouter } from 'react-router-dom'
import { randomIban } from '../../../../src/repository/repo';
class confirmCreditList extends React.Component {
    constructor(props) {
        super(props)
        this.state = {
            loading: false
        }
        //this.redirectPayment = this.redirectPayment.bind(this);
        // this.deleteConfirm = this.deleteConfirm.bind(this);
    }
    // redirectPayment() {
    //     const creditData = JSON.parse(localStorage.getItem('credit'));
    //     //get iban from bank
    //     randomIban().then(result => {
    //         creditData.tenantIban = result;
    //         localStorage.setItem('credit', JSON.stringify(creditData));
    //         this.props.history.push('/rent/paymentInstructions')
    //     }).catch(err => {})
    // }


    redirectPayment = (bankName) => {
        this.setState({ loading: true })
        this.state.loading = true
        const creditData = JSON.parse(localStorage.getItem('credit'));
        creditData.bankName = bankName;
        //get iban from bank
        randomIban().then(result => {

            creditData.tenantIban = result;
            localStorage.setItem('credit', JSON.stringify(creditData));
            this.props.history.push('/rent/paymentInstructions')
        }).catch(err => { })
            .finally(err => {
                setTimeout(
                    () => this.setState({ loading: true }),
                    1000
                );
            })
    }

    render() {
        const { creditData } = this.props
        return (
            <Loading loadingText={"Yükleniyor"} loading={this.state.loading}>
                {
                    <>

                        <CCard style={{ border: "1px solid #2eb85c" }}>
                            <CCardHeader>
                                {creditData.applicantName + ' ' + creditData.applicantSurname} ADINA BAŞVURU TALEBİ ALINMIŞTIR!
        
                    </CCardHeader>
                            <CCardBody>

                                <CRow>
                                    <CCol xs="3">
                                        <img src="https://ibar.az/img/logo.svg" className="h-35 d-block w-100-max" alt=""></img>
                                    </CCol>
                                    <CCol xs="6" style={{ borderLeft: "1px solid lightgray" }}>
                                        Kimlik Numarası:   {creditData.applicantIdentityNumber}
                                    </CCol>
                                    <CCol xs="3" style={{ fontWeight: "bold", fontSize: "18px" }}>
                                        <label>  {creditData.monthlyRentPrice * creditData.rentPeriod} TL</label>
                                        <br />
                                        <CButton onClick={() => this.redirectPayment('Ibar')} color="success">ONAYA GÖNDER!</CButton>
                                    </CCol>
                                </CRow>


                            </CCardBody>

                        </CCard>
                        <CCard style={{ border: "1px solid #2eb85c" }} >
                            <CCardHeader>
                                {creditData.applicantName + ' ' + creditData.applicantSurname} ADINA BAŞVURU TALEBİ ALINMIŞTIR!
        
                    </CCardHeader>
                            <CCardBody>
                                <CForm action="" method="post">
                                    <CRow>
                                        <CCol xs="3">
                                            <img style={{ background: "#1366b2", width: "190px", padding: "10px" }} id="logo" src="https://www.yapikredi.com.az/public/css/img/logo.png" alt="YapıKredi"></img>
                                        </CCol>
                                        <CCol xs="6" style={{ borderLeft: "1px solid lightgray" }}>
                                            Kimlik Numarası:   {creditData.applicantIdentityNumber}
                                        </CCol>
                                        <CCol xs="3" style={{ fontWeight: "bold", fontSize: "18px" }}>
                                            <label>   {creditData.monthlyRentPrice * creditData.rentPeriod} TL</label>
                                            <br />
                                            <CButton onClick={() => this.redirectPayment('YapıKredi')} color="success">ONAYA GÖNDER!</CButton>
                                        </CCol>
                                    </CRow>

                                </CForm>
                            </CCardBody>

                        </CCard>

                    </>
                }
            </Loading>
        )
    }
}

export default withRouter(confirmCreditList);

