import React, { useState } from 'react'
import {
  CButton,
  CCard,
  CCardBody,
  CCardHeader,
  CCol,
  CForm,
  CFormGroup,
  CInput,
  CInputGroup,
  CInputGroupPrepend,
  CSelect,
  CRow
} from '@coreui/react';
import '../../../css/paymentInstructions.css'
import CIcon from '@coreui/icons-react'
import { useHistory } from "react-router";
import { city, county } from '../../jsonFile/listCustomer';
import { Divider, Form, Label, Button, Icon, Checkbox, Card } from 'semantic-ui-react';
import 'semantic-ui-css/semantic.min.css';
import { getLandlordByIdentityNumber, saveContract } from '../../../../src/repository/repo';

const PaymentInstructions = () => {
  const [countyList, setCounty] = useState([{}]);
  const [landlordId, setLandlordId] = useState([{}]);
  const [loading, setLoading] = useState(false);
  const creditData = JSON.parse(localStorage.getItem('credit'));

  function getCounty(id) {
    var newCounty = county;
    const data = newCounty.filter(x => x.cityId === parseInt(id));
    setCounty(
      data
    );
  }
  const history = useHistory();
  const [landLordInfo, setLandLordInfo] = useState({
    identity: "",
    name: "",
    surname: "",
    accountNumber: "",
    phoneNumber: ""
  });

  function searchLandLord() {
    debugger;
    getLandlordByIdentityNumber(landlordId).then(result => {
      setLoading(true)
      setLandLordInfo({
        identity: result.identityNumber,
        name: result.name,
        surname: result.surname,
        accountNumber: result.ibans,
        phoneNumber : result.phoneNumber
      });
    }).catch(err => {setLoading(false) })
    .finally(err => {setLoading(false) })
  }

  function redirectConfirm() {
    history.push("/rent/rentProcess/confirmCredit")
  }

  function handleSubmit(event) {
    debugger;
    event.preventDefault();
    setLoading(true)
    var dataSave = {
      bankName: creditData.bankName,
      creditApplication: {
        id: creditData.id,
      },
      firstInstallmentDate: event.target["firstInstallmentDate"].value,
      landlord: {
        identityNumber: event.target["landlordIdentityNumber"].value,
        name: event.target["landlordName"].value,
        surname: event.target["landlordSurname"].value,
        phoneNumber: event.target["landlordPhoneNumber"].value,
      },
      landlordIban: event.target["landlordIban"].value,
      property: {
        address: event.target["propertyAddress"].value,
        city: city.filter(x => x.id === parseInt(event.target["propertyCity"].value))[0].name,
        country: "AZ",
        province: event.target["propertyCounty"].value,
        title: event.target["propertyCounty"].value + " / " + city.filter(x => x.id === parseInt(event.target["propertyCity"].value))[0].name
      },
      status: "STARTED",
      tenant: {
        identityNumber: event.target["tenantIdentityNumber"].value,
        name: event.target["tenantName"].value,
        phoneNumber: event.target["tenantPhoneNumber"].value,
        surname: event.target["tenantSurname"].value,
      },
      tenantIban: event.target["tenantIban"].value,
      user: {
        id: creditData.user.id,
      }
    }
    saveContract(dataSave).then(result => {
      setLoading(true)
      console.log("contract saved")
      console.log(result)
      history.push("/rent/rentProcess/confirmCredit");
    }).catch(err => {setLoading(false) })
    .finally(err => {setLoading(false) })
  }

  //credit data

  return (
    <>
      <CForm onSubmit={handleSubmit} className="form-horizontal">
        <CRow>
          <CCard style={{ width: "100%" }}>
            <CCardHeader style={{ color: "blue" }}>
              <Label style={{ background: "white", fontWeight: "600", fontSize: "14px", color: "blue" }}> KİŞİSEL BİLGİLER</Label>
            </CCardHeader>
            <CCardBody>
              <CRow>
                <CCol xs="12" md="6">
                  <CRow style={{ borderBottom: "1px solid #e7eaec", margin: '-5px 0 10px 0' }}>
                    <Label style={{ background: "white", fontWeight: "600", fontSize: "13px" }}>Kiracı Bilgileri</Label>
                  </CRow>
                  <CFormGroup row>
                    <CCol md="4">
                      <Label style={{ background: "white", fontWeight: "600", fontSize: "13px" }} htmlFor="hf-email">Kimlik No</Label>
                    </CCol>
                    <CCol xs="12" md="8">
                      <CInput value={creditData.applicantIdentityNumber} disabled={true} style={{ fontWeight: "600", fontSize: "13px" }} type="text" id="tenantIdentityNumber" name="tenantIdentityNumber" />
                    </CCol>
                  </CFormGroup>
                  <CFormGroup row>
                    <CCol md="4">
                      <Label style={{ background: "white", fontWeight: "600", fontSize: "13px" }} htmlFor="hf-email">Adı</Label>
                    </CCol>
                    <CCol xs="12" md="8">
                      <CInput value={creditData.applicantName} disabled={true} style={{ fontWeight: "600", fontSize: "13px" }} type="text" id="tenantName" name="tenantName" />
                    </CCol>
                  </CFormGroup>
                  <CFormGroup row>
                    <CCol md="4">
                      <Label style={{ background: "white", fontWeight: "600", fontSize: "13px" }} htmlFor="hf-password">Soyadı</Label>
                    </CCol>
                    <CCol xs="12" md="8">
                      <CInput value={creditData.applicantSurname} disabled={true} style={{ fontWeight: "600", fontSize: "13px" }} type="text" id="tenantSurname" name="tenantSurname" />
                    </CCol>
                  </CFormGroup>
                  <CFormGroup row>
                    <CCol md="4">
                      <Label style={{ background: "white", fontWeight: "600", fontSize: "13px" }} htmlFor="tenantIban">Hesap Numarası</Label>
                    </CCol>
                    <CCol xs="12" md="8">
                      <CInput type="text" value={creditData.tenantIban} disabled={true} style={{ fontWeight: "600", fontSize: "13px" }} id="tenantIban" name="tenantIban" />
                    </CCol>
                  </CFormGroup>
                  <CFormGroup row>
                    <CCol md="4">
                      <Label style={{ background: "white", fontWeight: "600", fontSize: "13px" }} htmlFor="tenantPhoneNumber">Telefon Numarası</Label>
                    </CCol>
                    <CCol xs="12" md="8">
                      <CInput type="text" value={creditData.applicationPhoneNumber} disabled={true} style={{ fontWeight: "600", fontSize: "13px" }} id="tenantPhoneNumber" name="tenantPhoneNumber" />
                    </CCol>
                  </CFormGroup>
                </CCol>
                <CCol xs="12" md="6" style={{ borderLeft: "1px solid #e7eaec" }}>
                  <CRow style={{ borderBottom: "1px solid #e7eaec", margin: '-5px 0 10px 0' }}>
                    <Label style={{ background: "white", fontWeight: "600", fontSize: "13px" }}>Konut Sahibi Bilgileri</Label>
                  </CRow>
                  <CFormGroup row>
                    <CCol md="4">
                      <Label style={{ background: "white", fontWeight: "600", fontSize: "13px" }} htmlFor="hf-email">Kimlik No</Label>
                    </CCol>
                    <CCol xs="12" md="8">
                      <CInputGroup>
                        <CInput id="landlordIdentityNumber" name="landlordIdentityNumber" onChange={e => setLandlordId(e.target.value)} />
                        <CInputGroupPrepend>
                          <CButton onClick={searchLandLord} type="button"
                            style={{ backgroundColor: "#2185d0" }} color="success">
                            <CIcon name="cil-magnifying-glass" /> Ara</CButton>
                        </CInputGroupPrepend>
                      </CInputGroup>
                    </CCol>
                  </CFormGroup>
                  <CFormGroup row>
                    <CCol md="4">
                      <Label htmlFor="landlordName" style={{ background: "white", fontWeight: "600", fontSize: "13px" }}>Adı</Label>
                    </CCol>
                    <CCol xs="12" md="8">
                      <CInput type="text" id="landlordName" name="landlordName" value={landLordInfo.name} />
                    </CCol>
                  </CFormGroup>
                  <CFormGroup row>
                    <CCol md="4">
                      <Label htmlFor="landlordSurname" style={{ background: "white", fontWeight: "600", fontSize: "13px" }} >Soyadı</Label>
                    </CCol>
                    <CCol xs="12" md="8">
                      <CInput type="text" id="landlordSurname" name="landlordSurname" value={landLordInfo.surname} />
                    </CCol>
                  </CFormGroup>
                  <CFormGroup row>
                    <CCol md="4">
                      <Label htmlFor="landlordIban" style={{ background: "white", fontWeight: "600", fontSize: "13px" }}>Hesap Numarası</Label>
                    </CCol>
                    <CCol xs="12" md="8">
                      <CInputGroup>
                        <CSelect
                          custom name="landlordIban" id="landlordIban">
                          <option>Seçiniz</option>
                          {landLordInfo && landLordInfo.accountNumber && landLordInfo.accountNumber.map(fbb =>
                            <option key={fbb} value={fbb}>{fbb}</option>)}
                        </CSelect>
                      </CInputGroup>
                    </CCol>
                  </CFormGroup>
                  <CFormGroup row>
                    <CCol md="4">
                      <Label style={{ background: "white", fontWeight: "600", fontSize: "13px" }} htmlFor="landlordPhoneNumber">Telefon Numarası</Label>
                    </CCol>
                    <CCol xs="12" md="8">
                      <CInput type="text" value={landLordInfo.phoneNumber} style={{ fontWeight: "600", fontSize: "13px" }} id="landlordPhoneNumber" name="landlordPhoneNumber" />
                    </CCol>
                  </CFormGroup>
                </CCol>
              </CRow>
            </CCardBody>
          </CCard>
        </CRow>

        <CRow>
          <CCard style={{ width: "100%" }}>
            <CCardHeader style={{ color: "blue" }}>
              <Label style={{ background: "white", fontWeight: "600", fontSize: "14px", color: "blue" }}> ÖDEME PLANI BİLGİLERİ</Label>
            </CCardHeader>
            <CCardBody>
              <CRow>
                <CCol xs="12" md="6">
                  <CFormGroup row>
                    <CCol md="4">
                      <Label htmlFor="rentPeriod" style={{ background: "white", fontWeight: "600", fontSize: "13px" }}>Taksit Adedi</Label>
                    </CCol>
                    <CCol xs="12" md="8">
                      <CInput type="text" value={creditData.rentPeriod} disabled={true} id="rentPeriod" name="rentPeriod" />
                    </CCol>
                  </CFormGroup>
                  <CFormGroup row>
                    <CCol md="4">
                      <Label htmlFor="firstInstallmentDate" style={{ background: "white", fontWeight: "600", fontSize: "13px" }}>İlk Taksit Tarihi</Label>
                    </CCol>
                    <CCol xs="12" md="8">
                      <CInput type="date" id="firstInstallmentDate" name="firstInstallmentDate" placeholder="date" />
                    </CCol>
                  </CFormGroup>
                </CCol>
                <CCol xs="12" md="6" style={{ borderLeft: "1px solid #e7eaec" }}>
                  <CFormGroup row>
                    <CCol md="4">
                      <Label style={{ background: "white", fontWeight: "600", fontSize: "13px" }} htmlFor="totalAmount">Toplam Tutar</Label>
                    </CCol>
                    <CCol xs="12" md="8">
                      <CInput type="text" value={creditData.rentPeriod * creditData.monthlyRentPrice} disabled={true} id="totalAmount" name="totalAmount" />
                    </CCol>
                  </CFormGroup>
                </CCol>
              </CRow>
            </CCardBody>
          </CCard>
        </CRow>

        <CRow>
          <CCard style={{ width: "100%" }}>
            <CCardHeader style={{ color: "blue" }}>
              <Label style={{ background: "white", fontWeight: "600", fontSize: "14px", color: "blue" }}> ADRES  BİLGİLERİ</Label>
            </CCardHeader>
            <CCardBody>
              <CRow>
                <CCol xs="12" md="6">
                  <CFormGroup row>
                    <CCol md="4">
                      <Label htmlFor="propertyCity" style={{
                        background: "white",
                        fontWeight: "600", fontSize: "13px"
                      }}>İl</Label>
                    </CCol>
                    <CCol xs="12" md="8">
                      <CSelect onChange={(e) => getCounty(e.target.value)}
                        custom name="propertyCity" id="propertyCity">
                        <option>Seçiniz</option>
                        {city && city.map(fbb =>
                          <option key={fbb.id} value={fbb.id}>{fbb.name}</option>)};
                         </CSelect>
                    </CCol>
                  </CFormGroup>
                </CCol>
                <CCol xs="12" md="6" style={{ borderLeft: "1px solid #e7eaec" }}>
                  <CFormGroup row>
                    <CCol md="4">
                      <Label style={{ background: "white", fontWeight: "600", fontSize: "13px" }}
                        htmlFor="propertyCounty">İlçe</Label>
                    </CCol>
                    <CCol xs="12" md="8">
                      <CSelect
                        custom name="propertyCounty" id="propertyCounty">
                        <option>Seçiniz</option>
                        {countyList && countyList.map(fbb =>
                          <option key={fbb.id} value={fbb.name}>{fbb.name}</option>)};
                         </CSelect>
                    </CCol>
                  </CFormGroup>
                </CCol>
                <CCol xs="12" md="12" style={{ marginTop: 10 }}>
                  <CFormGroup row>
                    <CCol md="2">
                      <Label htmlFor="propertyAddress" style={{
                        background: "white",
                        fontWeight: "600", fontSize: "13px"
                      }}>Adres</Label>
                    </CCol>
                    <CCol xs="12" md="10">
                      <textarea type="text" className="textArea" style={{}} id="propertyAddress" name="propertyAddress" placeholder="Adres Giriniz...." />
                    </CCol>
                  </CFormGroup>
                  <CCol xs="12" md="12" style={{ margin: "25px 0 0 0" }}>
                      <div className="float-right">
                        <div>
                          <Button content='İptal' negative type="button" />
                          <Button content='Kaydet' primary type="submit" />
                        </div>
                      </div>
                    </CCol>
                </CCol>
              </CRow>
            </CCardBody>
          </CCard>
        </CRow>


        {/* <CRow>
          <CCard style={{ width: "100%" }}>
            <CCardHeader style={{ color: "blue" }}>
              <Label style={{ background: "white", fontWeight: "600", fontSize: "14px", color: "blue" }}> BİLDİRİM BİLGİLERİ</Label>
            </CCardHeader>
            <CCardBody>
              <CRow>
                <CCol xs="12" md="6">
                  <CRow style={{ borderBottom: "1px solid #e7eaec", margin: '-5px 0 10px 0' }}>
                    <Label style={{ background: "white", fontWeight: "600", fontSize: "13px" }}>Bildirim Tercihleri</Label>
                  </CRow>
                  <CFormGroup row>
                    <CCol md="4">
                      <Label style={{ background: "white", fontWeight: "600", fontSize: "13px" }}>Ödeme Öncesi</Label>
                    </CCol>
                    <CCol md="8">
                      <CFormGroup variant="custom-radio" inline>
                        <Checkbox
                          radio
                          label='SMS'
                          name='checkboxRadioGroup'
                          value='this'
                        />
                      </CFormGroup>
                      <CFormGroup variant="custom-radio" inline>
                        <Checkbox
                          radio
                          label='E Posta'
                          name='checkboxRadioGroup'
                          value='this'
                        />
                      </CFormGroup>
                    </CCol>
                  </CFormGroup>
                  <CFormGroup row>
                    <CCol md="4">
                      <Label style={{ background: "white", fontWeight: "600", fontSize: "13px" }}>Ödeme Sonrası</Label>
                    </CCol>
                    <CCol md="8">
                      <CFormGroup variant="custom-radio" inline>
                        <Checkbox
                          radio
                          label='SMS'
                          name='checkboxRadioGroup'
                          value='this'
                        />
                      </CFormGroup>
                      <CFormGroup variant="custom-radio" inline>
                        <Checkbox
                          radio
                          label='E Posta'
                          name='checkboxRadioGroup'
                          value='this'
                        />
                      </CFormGroup>
                    </CCol>
                  </CFormGroup>
                </CCol>
                <CCol xs="12" md="6" style={{ borderLeft: "1px solid #e7eaec" }}>
                  <CRow style={{ borderBottom: "1px solid #e7eaec", margin: '-5px 0 10px 0' }}>
                    <Label style={{ background: "white", fontWeight: "600", fontSize: "13px" }}>İletişim Tercihleri</Label>
                  </CRow>
                  <CFormGroup row>
                    <CCol md="4">
                      <Label style={{ background: "white", fontWeight: "600", fontSize: "13px" }} htmlFor="hf-email">Cep Telefonu</Label>
                    </CCol>
                    <CCol xs="12" md="8">
                      <CInput type="password" id="hf-password" name="hf-password" autoComplete="current-password" />

                    </CCol>
                  </CFormGroup>
                  <CFormGroup row>
                    <CCol md="4">
                      <Label style={{ background: "white", fontWeight: "600", fontSize: "13px" }} htmlFor="hf-password">E Posta</Label>
                    </CCol>
                    <CCol xs="12" md="8">
                      <CInput type="password" id="hf-password" name="hf-password" autoComplete="current-password" />
                    </CCol>
                    <br />
                 
                  </CFormGroup>
                </CCol>
              </CRow>
            </CCardBody>
          </CCard>
        </CRow> */}
      </CForm>
    </>
  )
}

export default PaymentInstructions;
