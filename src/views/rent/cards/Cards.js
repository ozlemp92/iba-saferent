import React from 'react'
import {
  CButton,
  CCard,
  CCardBody,
  CCardFooter,
  CCardHeader,
  CCol,
  CCollapse,
  CDropdownItem,
  CDropdownMenu,
  CDropdownToggle,
  CFade,
  CForm,
  CFormGroup,
  CFormText,
  CValidFeedback,
  CInvalidFeedback,
  CTextarea,
  CInput,
  CInputFile,
  CInputCheckbox,
  CInputRadio,
  CInputGroup,
  CInputGroupAppend,
  CInputGroupPrepend,
  CDropdown,
  CInputGroupText,
  CLabel,
  CSelect,
  CRow
} from '@coreui/react'
import CIcon from '@coreui/icons-react'

const Cards = () => {
  return (
    <>
      <CRow>
        <CCol xs="12" md="6">
          <CCard >
            <CCardHeader style={{ color: "blue" }}>
              KİŞİSEL BİLGİLER
        </CCardHeader>
            <CCardHeader>
              <small>Kiracı Bilgileri</small>
            </CCardHeader>
            <CCardBody>
              <CForm action="" method="post" className="form-horizontal">
                <CFormGroup row>
                  <CCol md="3">
                    <CLabel htmlFor="hf-email">Adı</CLabel>
                  </CCol>
                  <CCol xs="12" md="9">
                    <CInput type="email" id="hf-email" name="hf-email" autoComplete="email" />
                  </CCol>
                </CFormGroup>
                <CFormGroup row>
                  <CCol md="3">
                    <CLabel htmlFor="hf-password">SoyAdı</CLabel>
                  </CCol>
                  <CCol xs="12" md="9">
                    <CInput type="password" id="hf-password" name="hf-password" autoComplete="current-password" />
                  </CCol>
                </CFormGroup>
                <CFormGroup row>
                  <CCol md="3">
                    <CLabel htmlFor="hf-password">Borçlu Hesap Numarası</CLabel>
                  </CCol>
                  <CCol xs="12" md="9">
                    <CInput type="password" id="hf-password" name="hf-password" autoComplete="current-password" />
                  </CCol>
                </CFormGroup>
              </CForm>
            </CCardBody>
          </CCard>
        </CCol>
        <CCol>
          <CCard>
            <CCardHeader>
              <small>Ev Sahibi Bilgileri</small>
            </CCardHeader>
            <CCardBody>
              <CForm action="" method="post" className="form-horizontal">
                <CFormGroup row>
                  <CCol md="3">
                    <CLabel htmlFor="hf-email">TC Kimlik No</CLabel>
                  </CCol>
                  <CCol xs="12" md="9">
                    <CInputGroup>
                      <CInputGroupPrepend>
                        <CButton type="button" color="primary"><CIcon name="cil-magnifying-glass" /> Ara</CButton>
                      </CInputGroupPrepend>
                      <CInput id="input1-group2" name="input1-group2" />
                    </CInputGroup>
                  </CCol>
                </CFormGroup>
                <CFormGroup row>
                  <CCol md="3">
                    <CLabel htmlFor="hf-password">Adı</CLabel>
                  </CCol>
                  <CCol xs="12" md="9">
                    <CInput type="password" id="hf-password" name="hf-password" autoComplete="current-password" />
                  </CCol>
                </CFormGroup>
                <CFormGroup row>
                  <CCol md="3">
                    <CLabel htmlFor="hf-password">Soy Adı</CLabel>
                  </CCol>
                  <CCol xs="12" md="9">
                    <CInput type="password" id="hf-password" name="hf-password" autoComplete="current-password" />
                  </CCol>
                </CFormGroup>
                <CFormGroup row>
                  <CCol md="3">
                    <CLabel htmlFor="hf-email">Bankalar</CLabel>
                  </CCol>
                  <CCol xs="12" md="9">
                    <CInputGroup>
                      <CInputGroupPrepend>
                        <CButton type="button" color="success"><CIcon name="cil-magnifying-glass" />Listele</CButton>
                      </CInputGroupPrepend>
                      <CInput id="input1-group2" placeholder="Lütfen Seçiniz" name="input1-group2" />
                    </CInputGroup>
                  </CCol>
                </CFormGroup>
              </CForm>
            </CCardBody>
          </CCard>

        </CCol>
      </CRow>

      <CRow>
        <CCol xs="12" md="6">
          <CCard >
            <CCardHeader style={{ color: "blue" }}>
              ÖDEME PLANI BİLGİLERİ
  </CCardHeader>
            <CCardBody>
              <CForm action="" method="post" className="form-horizontal">
                <CFormGroup row>
                  <CCol md="3">
                    <CLabel htmlFor="hf-email">Taksit Adedi</CLabel>
                  </CCol>
                  <CCol xs="12" md="9">
                    <CInput type="email" id="hf-email" name="hf-email" autoComplete="email" />
                  </CCol>
                </CFormGroup>
                <CFormGroup row>
                  <CCol md="3">
                    <CLabel htmlFor="hf-password">Depozito Tutarı</CLabel>
                  </CCol>
                  <CCol xs="12" md="9">
                    <CInput type="password" id="hf-password" name="hf-password" autoComplete="current-password" />
                  </CCol>
                </CFormGroup>
                <CFormGroup row>
                  <CCol md="3">
                    <CLabel htmlFor="hf-password">İlk Taksit Tarihi</CLabel>
                  </CCol>
                  <CCol xs="12" md="9">
                    <CInput type="date" id="date-input" name="date-input" placeholder="date" />
                  </CCol>
                </CFormGroup>
              </CForm>
            </CCardBody>
          </CCard>

        </CCol>
        <CCol xs="12" md="6">
          <CCard>
            <CCardBody>
              <CForm action="" method="post" className="form-horizontal">
                <CFormGroup row>
                  <CCol md="3">
                    <CLabel htmlFor="hf-email">Toplam Tutar</CLabel>
                  </CCol>
                  <CCol xs="12" md="9">
                    <CInput type="password" id="hf-password" name="hf-password" autoComplete="current-password" />
                  </CCol>
                </CFormGroup>
                <CFormGroup row>
                  <CCol md="3">
                    <CLabel htmlFor="hf-password">Komisyon Tutarı</CLabel>
                  </CCol>
                  <CCol xs="12" md="9">
                    <CInput type="password" id="hf-password" name="hf-password" autoComplete="current-password" />
                  </CCol>
                </CFormGroup>
              </CForm>
            </CCardBody>
          </CCard>

        </CCol>
      </CRow>

      <CRow>
        <CCol xs="12" md="6">
          <CCard >
            <CCardHeader style={{ color: "blue" }}>
              BİLDİRİM BİLGİLERİ
  </CCardHeader>
            <CCardHeader>
              <small>Bildirim Tercihleri</small>
            </CCardHeader>
            <CCardBody>
              <CForm action="" method="post" className="form-horizontal">
                <CFormGroup row>
                  <CCol md="4">
                    <CLabel>Ödeme Öncesi</CLabel>
                  </CCol>
                  <CCol md="8">
                    <CFormGroup variant="custom-radio" inline>
                      <CInputRadio custom id="inline-radio1" name="inline-radios" value="option1" />
                      <CLabel variant="custom-checkbox" htmlFor="inline-radio1">SMS</CLabel>
                    </CFormGroup>
                    <CFormGroup variant="custom-radio" inline>
                      <CInputRadio custom id="inline-radio2" name="inline-radios" value="option2" />
                      <CLabel variant="custom-checkbox" htmlFor="inline-radio2">E Posta</CLabel>
                    </CFormGroup>
                  </CCol>
                </CFormGroup>
                <CFormGroup row>
                  <CCol md="4">
                    <CLabel>Ödeme Sonrası</CLabel>
                  </CCol>
                  <CCol md="8">
                    <CFormGroup variant="custom-radio" inline>
                      <CInputRadio custom id="inline-radio1" name="inline-radios" value="option1" />
                      <CLabel variant="custom-checkbox" htmlFor="inline-radio1">SMS</CLabel>
                    </CFormGroup>
                    <CFormGroup variant="custom-radio" inline>
                      <CInputRadio custom id="inline-radio2" name="inline-radios" value="option2" />
                      <CLabel variant="custom-checkbox" htmlFor="inline-radio2">E Posta</CLabel>
                    </CFormGroup>
                  </CCol>
                </CFormGroup>
              </CForm>
            </CCardBody>
          </CCard>

        </CCol>
        <CCol xs="12" md="6">
          <CCard>
            <CCardHeader>
              <small>İletişim Tercihleri</small>
            </CCardHeader>
            <CCardBody>
              <CForm action="" method="post" className="form-horizontal">
                <CFormGroup row>
                  <CCol md="3">
                    <CLabel htmlFor="hf-email">Cep Telefonu</CLabel>
                  </CCol>
                  <CCol xs="12" md="9">
                    <CInput type="password" id="hf-password" name="hf-password" autoComplete="current-password" />

                  </CCol>
                </CFormGroup>
                <CFormGroup row>
                  <CCol md="3">
                    <CLabel htmlFor="hf-password">E Posta</CLabel>
                  </CCol>
                  <CCol xs="12" md="9">
                    <CInput type="password" id="hf-password" name="hf-password" autoComplete="current-password" />
                  </CCol>
                  <br /><br />
                  <CCol xs="12" md="9">
                      <CButton type="submit" size="sm" color="success"><CIcon name="cil-scrubber" />Kaydet</CButton> 
                  </CCol>
                </CFormGroup>
              </CForm>
            </CCardBody>
          </CCard>
        </CCol>
      </CRow>
    </>
  )
}

export default Cards
