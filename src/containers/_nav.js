export default [
  {
    _tag: 'CSidebarNavDropdown',
    name: 'Kredi İşlemleri',
    route: '/base',
    icon: 'cil-star',
    _children: [
      {
        _tag: 'CSidebarNavItem',
        name: 'Kira Kredi Başvurusu',
        to: '/rent/creditOperations',
      }
    ],
  },
  {
    _tag: 'CSidebarNavDropdown',
    name: 'Müşteri İşlemleri',
    route: '/buttons',
    icon: 'cil-star',
    _children: [
      {
        _tag: 'CSidebarNavItem',
        name: 'Müşteri Bilgileri',
        to: '/rent/customer/listCustomer',
      },  
    ],
  },
]

