import React from 'react'
import {
  CBadge,
  CDropdown,
  CDropdownItem,
  CDropdownMenu,
  CDropdownToggle,
  CImg
} from '@coreui/react'
import CIcon from '@coreui/icons-react';
import user from './../assets/icons/user.svg';
class TheHeaderDropdown extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
    };
    this.removeAccount = this.removeAccount.bind(this);
  }

  removeAccount = () => {
    localStorage.removeItem('userName');
    window.location.href = "/";
  }

  render() {
    return (
      <CDropdown
        inNav
        className="c-header-nav-items mx-2"
        direction="down"
      >
        <CDropdownToggle className="c-header-nav-link" caret={false}>
          <div className="c-avatar">
            <CImg
              src={user}
              className="c-avatar-img"
              alt="admin@bootstrapmaster.com"
            />
          </div>
        </CDropdownToggle>
        <CDropdownMenu className="pt-0" placement="bottom-end">
          <CDropdownItem
            header
            tag="div"
            color="light"
            className="text-center"
          >
            <strong>Hesap</strong>
          </CDropdownItem>
          <CDropdownItem>
            <CIcon name="cil-envelope-open" className="mfe-2" />
            Mesajlar
          <CBadge color="success" className="mfs-auto">42</CBadge>
          </CDropdownItem>
          <CDropdownItem
            header
            tag="div"
            color="light"
            className="text-center"
          >
            <strong>Ayarlar</strong>
          </CDropdownItem>
          <CDropdownItem>
            <CIcon name="cil-user" className="mfe-2" />
            Profil
        </CDropdownItem>
          <CDropdownItem>
            <CIcon name="cil-settings" className="mfe-2" />
            Ayarlar
        </CDropdownItem>
          <CDropdownItem divider />
          <CDropdownItem onClick={this.removeAccount} >
            <CIcon onClick={this.removeAccount} name="cil-lock-locked" className="mfe-2" />
            Çıkış
        </CDropdownItem>
        </CDropdownMenu>
      </CDropdown>
    )
  }
}

export default TheHeaderDropdown
