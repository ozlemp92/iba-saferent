import React from 'react'
import { CFooter } from '@coreui/react'

const TheFooter = () => {
  return (
    <CFooter fixed={false}>
      <div>
        <a href="https://saferent-landing.vercel.app" target="_blank" rel="noopener noreferrer">SafeRent</a>
        <span className="ml-1">&copy; 2020 trademark.</span>
      </div>
      <div className="mfs-auto">
        <span className="mr-1">Powered by Safe Rent Development Team</span>
      
      </div>
    </CFooter>
  )
}

export default React.memo(TheFooter)
