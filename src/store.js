import { createStore, applyMiddleware } from 'redux';
import { composeWithDevTools } 
from 'redux-devtools-extension/logOnlyInProduction';
import thunk from 'redux-thunk';
import rootReducer from './reducers';
const initialState = {
    sidebarShow: 'responsive'
  }
const changeState = (state = initialState, { type, ...rest }) => {
  switch (type) {
    case 'set':
      return {...state, ...rest }
    default:
      return state
  }
}
const middleware = [thunk];
// const store = createStore(
//  rootReducer,
//  initialState,
//  composeWithDevTools(applyMiddleware(...middleware))
// );
const store = createStore(changeState, 
    composeWithDevTools(applyMiddleware(...middleware)))
export default store;