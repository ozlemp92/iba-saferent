import React, { Component } from 'react';
import { HashRouter, Route, Switch,Router ,BrowserRouter} from 'react-router-dom';
import { Provider } from 'react-redux';
import history from './history';
import store from './store'
import './scss/style.scss';
const loading = (
  <div className="pt-3 text-center">
    <div className="sk-spinner sk-spinner-pulse"></div>
  </div>
)

// Containers
const TheLayout = React.lazy(() => import('./containers/TheLayout'));

// Pages
const Login = React.lazy(() => import('./views/pages/login/Login'));
const Register = React.lazy(() => import('./views/pages/register/Register'));
const Page404 = React.lazy(() => import('./views/pages/page404/Page404'));
const Page500 = React.lazy(() => import('./views/pages/page500/Page500'));
const WizardFormSecond = React.lazy(() => import('./views/rent/rentProcess/wizardFormSecond'));
class App extends Component {

  render() {
    return (
      <Provider store={store}>
        <BrowserRouter>
       
         <Router history={history}>
      <HashRouter>
          <React.Suspense fallback={loading}>
            <Switch>
              
            {<Route exact path="/" name="Login Page" 
            render={props => <Login {...props}/>} />}
               <Route exact path="/rent/rentProcess/wizardFormSecond"
                name="Confirm Credit Page" render={props => <WizardFormSecond 
                history={this.props.history} {...props}/>} />
              <Route exact path="/register" name="Register Page" render={props => <Register {...props}/>} />
              <Route exact path="/404" name="Page 404" render={props => <Page404 {...props}/>} />
              <Route exact path="/500" name="Page 500" render={props => <Page500 {...props}/>} />
              {
             <Route path="/" name="" render={props => <TheLayout {...props}/>} />}
            </Switch>
          </React.Suspense>
      </HashRouter>
      </Router>
      </BrowserRouter>
      </Provider>
    );
  }
}

export default App;
