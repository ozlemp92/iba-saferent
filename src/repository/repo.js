import axios from 'axios'
import {backend_address} from '../constant'

export const saveCredit = async (data) => {
    let result = await axios.post(backend_address + '/api/credit', data);
    return result.data;
}
export const sendSms = async (data) => {
    let result = axios.post(backend_address + '/api/sms/send', data);
    return result.data.data;
}

export const educationTypes = () => {
    return axios.get(backend_address + '/api/util/education').then(response => {
        return response.data;
    })
}

export const occupationTypes = () => {
    return axios.get(backend_address + '/api/util/occupation').then(response => {
        return response.data;
    })
}

export const randomIban = () => {
    return axios.get(backend_address + '/api/util/credit-iban').then(response => {
        return response.data;
    })
}

export const getLandlordByIdentityNumber = (idNumber) => {
    return axios.get(backend_address + '/api/landlord/' + idNumber).then(response => {
        return response.data;
    })
}

export const saveContract = async (data) => {
    let result = await axios.post(backend_address + '/api/contract/save', data);
    return result.data;
}

export const getCredit = (Id) => {
    let url = backend_address + '/api/credit/' + Id;
    return axios.get(url).then(response => {
        return response.data.data;
    })
}
export const getAllCredit = (Id) => {
    return axios.get(backend_address + '/api/credit/all').then(response => {
        return response.data.data;
    })
}

export const getContractByUserId = (userId) => {
    return axios.get(backend_address + '/api/contract/user/' + userId).then(response => {
        return response.data;
    })
}